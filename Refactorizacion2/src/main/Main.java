package main;
/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */
//Solo se debe declarar unicamente los paquetes que vallamos a emplear
import java.util.Scanner;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Main {

	static Scanner in;
	public static void main(String[] args) 
	{
		in = new Scanner(System.in);
		//Cada declaracion de variable en una linea
		int cantidad_maxima_alumnos;
		cantidad_maxima_alumnos = 10;
		//Declara variables lo m�s cercano a su uso
		int n;
		int arrays[] = new int[10];
		for( n=0 ; n<10 ;n++ ){
			//No se puede declarar un int como String
			System.out.println("Introduce nota media de alumno");
			arrays[n] = in.nextInt();
		}	
		//Falta un parentesis en esta declarcion
		System.out.println("El resultado es: " + recorrer_array(arrays));
		//Para salir de la clase usamos System.exit(0)
		System.exit(0);;
	}
	//Los arrays deben estar declarados justo despues del tipo de variable
	static double recorrer_array(int[] vector)
	{
		//Debemos inizializar la variable para que acumule diferentes valores
		double c = 0;
		for( int a=0 ; a<10 ;a++ ) 
		{
			c=c+vector[a];
		}
		return c/10;
	}
	
}