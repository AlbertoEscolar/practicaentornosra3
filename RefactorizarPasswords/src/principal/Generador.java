package principal;

import java.util.Scanner;

public class Generador {
	
	// Codigo Duplicado y Metodos Largos
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		//Unificamos el c�digo que es igual en varios metodos unificados
		
		menu();
		
	}

	private static void menu() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Programa que genera passwords de la longitud indicada,"
				+ " y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = scanner.nextInt();
		//Agrupamos otro metodo enlace con un metodo que englobe el switch
		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();
		String password = "";
		
		password = posiblesCasos(longitud, opcion, password);
		//Resultado de la CONTRASE�A
		System.out.println(password);
		scanner.close();
		
	}

	private static String posiblesCasos(int longitud, int opcion, String password) {
		
		password = diferentesContras(longitud, opcion, password);
		
		return password;
	}

	private static String diferentesContras(int longitud, int opcion, String password) {
		switch (opcion) {
		case 1:
			for (int i = 0; i < longitud; i++) {
				password = contra1(password);
			}
			break;
		case 2:
			for (int i = 0; i < longitud; i++) {
				password = contra2(password);
			}
			break;
		case 3:
			for (int i = 0; i < longitud; i++) {
				password = contra3(password);
			}
			break;
		case 4:
			for (int i = 0; i < longitud; i++) {
				password = contra4(password);
			}
			break;
		}
		return password;
	}

	private static String contra1(String password) {
		char letra1;
		letra1 = (char) ((Math.random() * 26) + 65);
		password += letra1;
		return password;
	}

	private static String contra2(String password) {
		int numero2;
		numero2 = (int) (Math.random() * 10);
		password += numero2;
		return password;
	}

	private static String contra3(String password) {
		int n;
		n = (int) (Math.random() * 2);
		if (n == 1) {
			password = contra1(password);
		} else {
			char caracter3;
			caracter3 = (char) ((Math.random() * 15) + 33);
			password += caracter3;
		}
		return password;
	}

	private static String contra4(String password) {
		int n;
		n = (int) (Math.random() * 3);
		if (n == 1) {
			password = contra1(password);
		} else if (n == 2) {
			char caracter4;
			caracter4 = (char) ((Math.random() * 15) + 33);
			password += caracter4;
		} else {
			password = contra2(password);
		}
		return password;
	}

}
