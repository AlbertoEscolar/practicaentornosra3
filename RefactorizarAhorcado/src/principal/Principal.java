package principal;

import java.io.File;
import java.util.Scanner;

public class Principal {
	private static final byte NUM_PALABRAS = 20;
	private static final byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];

	public static void main(String[] args) {

		String palabraSecreta, ruta = "src\\palabras.txt";
		
		File fich = new File(ruta);
		Scanner inputFichero = null;
		//Metodo para importar el fichero
		importarFichero(fich, inputFichero);

		Scanner input = new Scanner(System.in);
		
		palabraSecreta = extraerPalabra();

		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];

		String caracteresElegidos = "";
		int fallos;
		boolean acertado;
		System.out.println("Acierta la palabra");
		
		caracteresElegidos = aciertosYFallos(palabraSecreta, input, caracteresPalabra, caracteresElegidos);

		input.close();
	}

	private static String aciertosYFallos(String palabraSecreta, Scanner input, char[][] caracteresPalabra,
			String caracteresElegidos) {
		int fallos;
		boolean acertado;
		do {

			System.out.println("####################################");

			caracteresElegidos = menu(input, caracteresPalabra, caracteresElegidos);
			
			fallos = 0;

			boolean encontrado;
			
			fallos = buscarPalabra(caracteresPalabra, caracteresElegidos, fallos);

			interfazSegunFallos(fallos);

			acertado = resultadoDePalabra(palabraSecreta, caracteresPalabra, fallos);

		} while (!acertado && fallos < FALLOS);
		return caracteresElegidos;
	}

	private static String extraerPalabra() {
		return palabras[(int) (Math.random() * NUM_PALABRAS)];
	}

	private static String menu(Scanner input, char[][] caracteresPalabra, 
			String caracteresElegidos) {
		for ( int i = 0; i < caracteresPalabra[0].length ; i++ ) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();

		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
		caracteresElegidos += input.nextLine().toUpperCase();
		return caracteresElegidos;
	}

	private static int buscarPalabra(char[][] caracteresPalabra, String caracteresElegidos, int fallos) {
		boolean encontrado;
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			encontrado = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					encontrado = true;
				}
			}
			if (!encontrado)
				fallos++;
		}
		return fallos;
	}

	private static boolean resultadoDePalabra(String palabraSecreta, char[][] caracteresPalabra, int fallos) {
		boolean acertado;
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraSecreta);
		}
		acertado = true;
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		if (acertado)
			System.out.println("Has Acertado ");
		return acertado;
	}

	private static void interfazSegunFallos(int fallos) {
		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:
			errores2();
			break;
		case 3:
			System.out.println("  ____ ");
			errores2();
			break;
		case 4:
			errores4();
			break;
		case 5:
			errores5();
			break;
		case 6:
			errores6();
			break;
		case 7:
			errores7();
			break;
		}
	}

	private static void errores7() {
		System.out.println("  ____ ");
		System.out.println(" |    |");
		System.out.println(" O    |");
		System.out.println(" T    |");
		System.out.println(" A   ___");
	}

	private static void errores6() {
		System.out.println("  ____ ");
		System.out.println(" |    |");
		System.out.println(" O    |");
		System.out.println(" T    |");
		System.out.println("     ___");
	}

	private static void errores5() {
		System.out.println("  ____ ");
		System.out.println(" |    |");
		System.out.println(" O    |");
		System.out.println("      |");
		System.out.println("     ___");
	}

	private static void errores4() {
		System.out.println("  ____ ");
		System.out.println(" |    |");
		System.out.println("      |");
		System.out.println("      |");
		System.out.println("     ___");
	}

	private static void errores2() {
		System.out.println("      |");
		System.out.println("      |");
		System.out.println("      |");
		System.out.println("     ___");
	}

	private static void importarFichero(File fich, Scanner inputFichero) {
		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}
	}

}